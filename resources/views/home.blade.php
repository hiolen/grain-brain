@extends('layouts.app')

@section('content')
<div class="sTop rel">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <img src="{{asset('assets/images/book.png')}}" alt="Book">
            </div>
            <div class="col-xs-12 col-sm-6 sTop-right">
                <h1>Grain Brain</h1>
                <span>The Surprising Truth about Wheat, Carbs, and Sugar Your Brain's Silent Killers</span>
                <p>A #1 New York Times best-seller paves the way for its readers to know the important information about the human brain – how it works and how to take proper care of it.</p>
                <ul class="nav navbar-nav">
                    <li><a class="btn btn-primary" href="#">Hardcover<br><small> $16.20</small></a></li>
                    <li><a class="btn btn-primary" href="#">Hardcover<br><small> $16.20</small></a></li>
                    <li><a class="btn btn-primary" href="#">Hardcover<br><small> $16.20</small></a></li>
                </ul>
                <p class="asterisk"><i class="fa fa-asterisk"></i>  you can find more details about each version below</p>
            </div>
        </div>
    </div>
</div>

<div class="section  s1">
    <div class="container">
        <div class="text-center">
            <span class="intro">Introducing</span>
            <h1>Grain Brain</h1>
            <h4><em>The Surprising Truth about Wheat, Carbs, and Sugar Your Brain's Silent Killers</em></h4>
        </div>
        <div class="des des1">
            <p>A #1 New York Times best-seller paves the way for its readers to know the important information about the human brain - how it works and how to take proper care of it.</p>
            <p>What the readers will learn from this book can be quite a shock. Carbohydrates - something that people have thought of as an important part of their daily diet - has negative effects on the brain.</p>
            <p>Carbohydrates have the ability to destroy the human brain and it is a fact that has unfortunately been buried in medical literature for quite too long. With David Perlmutter's book, you will know the truth behind what this silent killer can do to your brain, along with the gluten content in wheat and the irresistable tempation: sugar.</p>
            <p>Even the healthiest-sounding whole grains can cause ADHD, anxiety and depression, chronic headaches, and dementia. Grain brain will teach you how to beat these silent killers and build a healthy brain as fast as 4 weeks,</p>
            <p>Buy Grain Brain: The Surprising Truth about Wheat, Carbs, and Sugar--Your Brain's Silent Killers on Amazon.com for as low as $13.60. Grab a copy today!</p>
            <a class="btn-primary btn-primary-block" href="#">Buy Now</a>
            <div class="rel post">  
                <img src="{{asset('assets/images/amber.png')}}" alt="Author's Photo">
                <p><em>"Interesting, Surprising, and Mindblowing."</em></p>
                <span class="author-name"><em>- Amber Howell</em></span>
            </div>
        </div>
    </div>
</div>

<div class="section s2">
    <div class="container">
        <div class="text-center">
            <span class="intro">Grain Brain</span>
            <h3 class="title">Features & Benefits of Grain Brain</h3>
        </div>
        <div class="des">
            <div class="word">
                <img class="circle" src="{{asset('assets/images/amber.png')}}" alt="Author's Photo">
                <div class="word1">
                <h4>Take Control of Your Mind and Body</h4>
                <p>Say goodbye to grain brain and start taking charge of your mind and body. The book includes dietary habits and steps on how to build a better brain the natural way. You will learn the right foods to eat and the right activities to do in order to be a healthier and happier you.</p>
                </div>  
                
            </div>
            <div class="word">
                <img class="circle" src="{{asset('assets/images/amber.png')}}" alt="Author's Photo">
                <div class="word1">
                <h4>Open Your Eyes to The Truth</h4>
                <p>This information-rich book will open the readers’ eyes to the devastating truth that carb consumption holds. Grain Brain will help the readers know who you brain’s worst enemies are so that you can avoid them as early as you can. Your life will change once you read this eye-opening book.</p>
                </div>  
            </div>
            <div class="word">
                <img class="circle" src="{{asset('assets/images/amber.png')}}" alt="Author's Photo">
                <div class="word1">
                <h4>Reprogram Your Genetic Destiny</h4>
                <p>Grain Brain will take your brain to rehab and that’s not a bad thing! The book will help you live a new and healthier way of life in a span of only 4 weeks. It also includes meal plans and recipes that will help you jumpstart your way to optimum health.</p>
                </div>
            </div>
            <div class="rel post">  
                <img src="{{asset('assets/images/amber.png')}}" alt="Author's Photo">
                <p><em>"Interesting, Surprising, and Mindblowing."</em></p>
                <span class="author-name"><em>-Amber Howell</em></span>
            </div>
        </div>
    </div>
</div>

<div class="section s3">
    <div class="container">
        <div class="text-center">
            <span class="intro">Here's what your getting</span>
            <h3>Synopsis</h3>
            <h4><em>A small except from the book that you might get interested.</em></h4>
        </div>
        <div class="des">
            <p>Grain Brain will give you 336 pages and 11 chapters worth of knowledge about the most important and most fragile organ in you body, the brain. You will learn the causes of brain diseases such as inflammation and many more. Various illnesses that occur in the brain are inevitable but this book holds numerous methods of preventing these illnesses from occuring.</p>
            <p>The brain is the body's central processing unit. If the brain is not functioning properly, the body would follow suit, which is exactly the reason why there is a need for proper care of this vital organ. This book will help you do just that.</p>
            <p>The chapters of this book are filled with important information that YOU NEED TO KNOW.</p>
            <p>Chapters 1 to 6 focus on what you need to know about brain diseases, the effects of carbohydrates, gluten, fat, and sugar to your brain, and how to change your genetic destiny.</p>
            <p>Chapters 7 to 9 focus on the importance of sleep, the dietary habits to be observed for optimal brain, and the steps on building a better brain through exercise.</p>
            <p>Chapters 10 to 11  focus on taking action for anew way of life and eating your way to a healthy and strong brain.</p>
            <p>What's good about this book is that its contents were carefully research by Dr. David Perlmutter, who is trusted by many in the medical field.<b> Grain Brain: The Surprising Truth about Wheat, Carbs, and Sugar--Your Brain's Silent Killers</b> also focuses on prevention because after all, prevention is always better than the cure.</p>
            <a class="btn-primary btn-primary-block" href="#">Download a free sample now <span class="text-lowercase">(.pdf)</span></a>
        </div>  
    </div>
</div>

<div class="section s4">
    <div class="container">
        <div class="text-center">
            <span class="intro">Here's what their saying about this book</span>
            <h3>Testimonials</h3>
        </div>
        <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col">
                <img src="{{asset('assets/images/amber.png')}}">
                <div class="s4-des">
                    <h5>Tiffany Kinder</h5>
                    <p>"Grain Brain has helped me change my lifestyle and eating habits into a better one. I have always thought that carbs were the right choice for my body and mind but never have i been so wrong. I am so glad that I found thiis book because it has opened my eyes to the truth."</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col">
                <img src="{{asset('assets/images/amber.png')}}">
                <div class="s4-des">
                    <h5>Tiffany Kinder</h5>
                    <p>"Grain Brain has helped me change my lifestyle and eating habits into a better one. I have always thought that carbs were the right choice for my body and mind but never have i been so wrong. I am so glad that I found thiis book because it has opened my eyes to the truth."</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col">
                <img src="{{asset('assets/images/amber.png')}}">
                <div class="s4-des">
                    <h5>Tiffany Kinder</h5>
                    <p>"Grain Brain has helped me change my lifestyle and eating habits into a better one. I have always thought that carbs were the right choice for my body and mind but never have i been so wrong. I am so glad that I found thiis book because it has opened my eyes to the truth."</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col">
                <img src="{{asset('assets/images/amber.png')}}">
                <div class="s4-des">
                    <h5>Tiffany Kinder</h5>
                    <p>"Grain Brain has helped me change my lifestyle and eating habits into a better one. I have always thought that carbs were the right choice for my body and mind but never have i been so wrong. I am so glad that I found thiis book because it has opened my eyes to the truth."</p>
                </div>
            </div>
        </div>
        </div>
        <div class="text-center buy-now">   
        <p>Call to action text and description goes here to intice buyers</p>
        <a class="btn-primary"href="#">BUY NOW</a>
        
        </div>
        <div class="rel post">  
                <img src="{{asset('assets/images/amber.png')}}">
                <p> “Interesting, Surprising, and Mindblowing.”</p>
                <span>-Amber Howell</span>
        </div>

    </div>
</div>

<div class="text-center banner">
    <div class="container">
        <h5>How Would You Like to Get a Copy of this Eye-Opening and Life-Saving Book?</h5>
        <p>Learn by ear through purchasing the Audiobook version of this book, or keep it handy by purchasing the Hardcover or </p>
        <p>Paperback version. The choice is yours. Make it now. Grab a copy today!</p>
        <a class="btn-primary btn-primary-block" href="#">Download Now</a>
    </div>
</div>

<div class="section s5">
    <div class="container">
        <div class="des">
            <h1 class="text-center">About the Author</h1>
            <span class="intro text-center block">Some facts and bio about the author</span>
            <img src="{{asset('assets/images/amber.png')}}" alt="Author's Photo">
            <h4 class="text-center">David Perlmutter<span class="prof"> MD, FACN, ABIHM</span></h4>
            <p>A renowned Neurologist and Fellow of the American College of Nutrition who has made extensive contributions in medical literature. He hold lectures sponsored by well-known medical institutions as he is recognized internationally as a leader in the fields of neurological disorders.</p>
            <p class="award">He has received multiple awards such as  the following:</p>
            <ul class="award-rec.">
                <li><span>Leonard G. Rowntree Research Award</span></li>
                <li><span>Linus Pauling Award (2002)</span></li>
                <li><span>National Nutritional Foods Association Clinician of the Year Award (2006)</span></li>
            </ul>
            <p>He has appeared in several radio and televison programs such as 20/20, CNN, Larry King Live, Fox News, Fox and Friends, Oprah, The CBS Early Show, and The Today Show. Dr. Perlmutter is also a medical adviser for The Dr. Oz show.</p>
        </div>
    </div>
</div>

<div class="section related">
    <div class="container">
        <div class="clearfix">
            <h3>Related Books</h3>
            <a href="#" class="visible-xs"><i class="fa fa-link"></i></a>
            <a class="btn-primary f-right hidden-xs" href="#">view all books</a>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <img src="{{asset('assets/images/book1.png')}}" alt="Book">
                <h4>BOOK ONE</h4>
                <p>Some description about book number one goes here and other details. Some description about book number one goes here and other details.</p>
                <a class="btn-primary btn-primary-block" href="#">view details book</a>
            </div>
            <div class="col-xs-12 col-sm-4">
                <img src="{{asset('assets/images/book1.png')}}" alt="Book">
                <h4>BOOK ONE</h4>
                <p>Some description about book number one goes here and other details. Some description about book number one goes here and other details.</p>
                <a class="btn-primary btn-primary-block" href="#">view details book</a>
            </div>
            <div class="col-xs-12 col-sm-4">
                <img src="{{asset('assets/images/book1.png')}}" alt="Book">
                <h4>BOOK ONE</h4>
                <p>Some description about book number one goes here and other details. Some description about book number one goes here and other details.</p>
                <a class="btn-primary btn-primary-block" href="#">view details book</a>
            </div>
        </div>
    </div>
</div>
@endsection
