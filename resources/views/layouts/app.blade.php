<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name = "keywords" content = "SMINT jQuery Plugin, SMINT, jQuery Plugin, One Page Website, Sticky Navigation" />
    <meta name = "description" content = "SMINT is a simple plugin for lovers of one page websites, which helps with sticky menus and page scrolling." />

    <title>Grain Brain</title>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/font-awesome.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/demo.css')}}"/>

</head>
<body id="app-layout">
    <div class="wrap ">
        <div class="header-menu">
            <div class="subMenu" >
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#HeaderMenu">
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="h2 logo">Brain Grain</div>
                    </div>
                    <div class="collapse navbar-collapse menu-collapse" id="HeaderMenu">
                        <ul class="nav navbar-nav main-nav">
                            <a href="#sTop" class="subNavBtn">Home</a>
                            <a href="#s1" class="subNavBtn">LESSONS</a> 
                            <a href="#s2" class="subNavBtn">REVIEWS</a>
                            <a href="#s3" class="subNavBtn">SYNOPSIS</a>
                            <a href="#s4" class="subNavBtn">EXCERPT</a>
                            <a href="#s5" class="subNavBtn">AUTHOR</a>  
                        </ul>
                        <ul class="nav navbar-nav navbar-right social">
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                            <a href="#"><i class="fa fa-envelope-o"></i></a>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        @yield('content')

    </div>

    <!-- JavaScripts -->
    <script src="{{asset('assets/js/jquery-1.12.0.min.js')}}"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.smint.js')}}" type="text/javascript" ></script>
    <script src="{{asset('assets/js/script.js')}}"></script>
</body>
</html>
